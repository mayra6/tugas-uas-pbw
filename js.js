var selectedRow = null;

// show alerts
function showAlerts(message, classname){
    const div = document.createElement("div");
    div.classname = `alert alert-${classname}`;

    div.appendchild(document.createTextNode(message));
    const container = document.querySelector(".container");
    const main = document.querySelector(".main");
    container.insertBefore(div, main);

    setTimeout(() => document.querySelector(".alert").remove(), 3000);
}

// Clear all fileds
function clearfields(){
    document.querySelector("#nomer").value = "";
    document.querySelector("#makan").value = "";
    document.querySelector("#minum").value = "";
}

// Add data
document.querySelector("#menu-form").addEventListener("submit",(e) =>{
    e.preventDefault();

    // GET FORM VALUES
    const nomer = document.querySelector("#nomer").value;
    const makan = document.querySelector("#makan").value;
    const minum = document.querySelector("#minum").value;

    // validate
    if(nomer == "" || makan == "" || minum == ""){
        
    }
    else{
        if(selectedRow == null){
            const list = document.querySelector("#menu-list");
            const row = document.createElement("tr");

            row.innerHTML = `
                <td>${nomer}</td>
                <td>${makan}</td>
                <td>${minum}</td>
                <td>
                <a href="#" class="btn btn-warning btn-sm edit">EDIT</a>
                <a href="#" class="btn btn-danger btn-sm delete">DELETE</a>
                `;
                list.appendChild(row);
                selectedRow = null;
                
        }
        else{
            selectedRow.children[0].textContent = nomer;
            selectedRow.children[1].textContent = makan;
            selectedRow.children[2].textContent = minum;
            selectedRow = null;
            
        }

        clearfields();
    }
});

// Edit data

document.querySelector("#menu-list").addEventListener("click", (e) =>{
    target = e.target;
    if(target.classList.contains("edit")){
        selectedRow = target.parentElement.parentElement;
        document.querySelector("#nomer").value = selectedRow.children[0].textContent
        document.querySelector("#makan").value = selectedRow.children[1].textContent
        document.querySelector("#minum").value = selectedRow.children[2].textContent
    }
});

// Delete data

document.querySelector("#menu-list").addEventListener("click", (e) =>{
    target = e.target;
    if(target.classList.contains("delete")){
        target.parentElement.parentElement.remove();
    }
});